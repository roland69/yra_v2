    <section class="parallax-section clearfix service_template" id="features">
        <div class="mid-content">
            <h1><span>Our Focus</span></h1>
            <div class="parallax-content">
                <div class="page-content">
                    <p style="text-align: center;"><?php _e('Curabitur eget interdum risus. Curabitur dictum, libero ut mattis consequat, lectus mauris congue risus, et volutpat nulla eros vel arcu.','accesspress-parallax'); ?></p>
                </div>
            </div> 
            <div class="service-listing clearfix">
                <div class="clearfix service-list odd wow fadeInLeft" data-wow-delay="0.25s">
                    <div class="service-image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/demo/icon1.png" alt="EASY THEME OPTION PANEL">
                    </div>
                    <div class="service-detail">
                        <h3>ACCOUNTING</h3>
                        <div class="service-content"><p><?php _e( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis metus vitae ligula elementum ut luctus lorem facilisis.', 'accesspress-parallax' ); ?></p>
                        </div>
                    </div>
                </div>
                <div class="clearfix service-list even wow fadeInRight" data-wow-delay="0.5s">
                    <div class="service-image">
                        <img src="<?php
                        echo get_template_directory_uri();
                        ?>/images/demo/icon2.png" alt="EASY CUSTOMIZABLE">
                    </div>
                    <div class="service-detail">
                        <h3>TAXATION</h3>
                        <div class="service-content"><p><?php _e( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis metus vitae ligula elementum ut luctus lorem facilisis.', 'accesspress-parallax' ); ?></p>
                        </div>
                    </div>
                </div>
                <div class="clearfix service-list odd wow fadeInLeft" data-wow-delay="0.75s">
                    <div class="service-image">
                        <img src="<?php
                        echo get_template_directory_uri();
                        ?>/images/demo/icon3.png" alt="CLEAN CODING">
                    </div>
                    <div class="service-detail">
                        <h3>PAYROLL</h3>
                        <div class="service-content"><p><?php _e( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis metus vitae ligula elementum ut luctus lorem facilisis.', 'accesspress-parallax' ); ?></p>
                        </div>
                    </div>
                </div>
                <div class="clearfix service-list even wow fadeInRight" data-wow-delay="1s">
                    <div class="service-image">
                        <img src="<?php
                        echo get_template_directory_uri();
                        ?>/images/demo/icon4.png" alt="HTML5 &#038; CSS3">
                    </div>
                    <div class="service-detail">
                        <h3>BOOK KEEPING</h3>
                        <div class="service-content"><p><?php _e( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis metus vitae ligula elementum ut luctus lorem facilisis.', 'accesspress-parallax' ); ?></p>
                        </div>
                    </div>
                </div>
                <div class="clearfix service-list odd wow fadeInLeft" data-wow-delay="1.25s">
                    <div class="service-image">
                        <img src="<?php
                        echo get_template_directory_uri();
                        ?>/images/demo/icon5.png" alt="7X24 SUPPORT">
                    </div>
                    <div class="service-detail">
                        <h3>WEB DEVELOPMENT</h3>
                        <div class="service-content"><p><?php _e( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis metus vitae ligula elementum ut luctus lorem facilisis.', 'accesspress-parallax' ); ?></p>
                        </div>
                    </div>
                </div>
                <div class="clearfix service-list even wow fadeInRight" data-wow-delay="1.5s">
                    <div class="service-image">
                        <img src="<?php
                        echo get_template_directory_uri();
                        ?>/images/demo/icon6.png" alt="MULTIPURPOSE">
                    </div>
                    <div class="service-detail">
                        <h3>IT SOLUTIONS</h3>
                        <div class="service-content"><p><?php _e( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis metus vitae ligula elementum ut luctus lorem facilisis.', 'accesspress-parallax' ); ?></p>
                        </div>
                    </div>
                </div>
            </div><!-- #primary -->
        </div>
    </section>
    <section class="parallax-section clearfix portfolio_template" id="portfolio">
        <div class="overlay"></div>
        <div class="mid-content">
            <h1><span>YRA OUTSOURCING AND BUSINESS SOLUTIONS</span></h1>
            <div class="parallax-content">
                <div class="page-content">
                    <p style="text-align: center;"><?php _e( 'Your friendly and innovative company committed to provide a broad range of quality but affordable outsourcing services and business solutions.   ', 'accesspress-parallax' ); ?></p>
                </div>
                <div class="clearfix btn-wrap">
                    <a class="btn" href="#"><?php _e( 'Contact Us', 'accesspress-parallax' ); ?></a>
                </div>
            </div>
        </div>
    </section>
    <section class="parallax-section clearfix testimonial_template" id="testimonials">
        <div class="mid-content">
            <h1><span><?php _e( 'Testimonials', 'accesspress-parallax' ); ?></span></h1>
            <div class="parallax-content">
            </div> 
            <div class="testimonial-listing clearfix wow fadeInUp">
                <div class="testimonial-slider">
                    <div class="testimonial-list">
                        <div class="testimonial-content"><p><?php _e( 'Thanks for delivering top quality services to your clients. It just takes a minute to get an answer from you when in difficulties. I use the chat service and I can say that the AccessPress Themes staff knows how to shorten the physical distance to its clients.<br />Thanks again for making our experience with AccessPress Themes the best one online!', 'accesspress-parallax' ); ?></p>
                        </div>
                        <h3><?php _e( 'Yanetxys Torreblanca', 'accesspress-parallax' ); ?></h3>
                        <div class="testimonial-image">
                            <img src="<?php echo get_template_directory_uri();?>/images/demo/testimonial1.jpg" alt="Yanetxys Torreblanca">
                        </div>
                    </div>
                    <div class="testimonial-list">
                        <div class="testimonial-content"><p><?php _e( 'Thank you very much the support team accesspress lite for service, are really wonderful in their care and in the resolution of the problem. Congratulations to facebook chat and Junu team.', 'accesspress-parallax' ); ?></p>
                        </div>
                        <h3><?php _e( 'David Soriano', 'accesspress-parallax' ); ?></h3>
                        <div class="testimonial-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/demo/testimonial2.jpg" alt="David Soriano">
                        </div>
                    </div>
                    <div class="testimonial-list">
                        <div class="testimonial-content"><p><?php _e( 'Great Customer service and technical assistance!!!<br />
                                The template is really good and nice, definitely worth going PRO', 'accesspress-parallax' ); ?></p>
                        </div>
                        <h3><?php _e( 'Stefano Roganti', 'accesspress-parallax' ); ?></h3>
                        <div class="testimonial-image">
                            <img src="<?php echo get_template_directory_uri();?>/images/demo/testimonial3.jpg" alt="Stefano Roganti">
                        </div>
                    </div>
                </div>
            </div><!-- #primary -->
        </div>
    </section>
   <section class="parallax-section clearfix portfolio_template" id="contact-us">
        <div class="overlay"></div>
        <div class="mid-content">
            <div class="portfolio-listing clearfix">
                <div class="portfolio-list">

            <h2>Site Links:</h2>          
                <ul>
                    <li><a href="#"  style="color: #fff"><?php _e('Home','guardian'); ?></a></li>
                    <li><a href="#"  style="color: #fff"><?php _e('Service','guardian'); ?></a></li>
                    <li><a href="#"  style="color: #fff"><?php _e('About-us','guardian'); ?></a></li>
                    <li><a href="#"  style="color: #fff"><?php _e('Contact -Us','guardian'); ?></a></li>
                </ul>   
                </div>
                <div class="portfolio-list">
<h2>Contact Info:</h2>
<strong>Email:</strong>  yra-email@gmail.com<br>
            <strong>Phone Number:</strong> 123-456-789 <br>
            <strong>Address:</strong>       Bldg 10, Unit 408, 8990 Campville,
          SLEX East Service Road,
          Barangay Cupang, Muntinlupa City, Philippines  
                </div>
                 <div class="portfolio-list">
                    <h2>About Us:</h2>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </div>
                  <div class="portfolio-list">
<img src="<?php
                        echo get_template_directory_uri();
                        ?>/images/demo/yrawhite.png" alt="yra logo">
                </div>
             
            </div><!-- #primary -->
        </div>
    </section>
</div>

<style type='text/css' media='all'>#features{ background:url() no-repeat scroll top left #f6f6f6; background-size:cover; color:#333333}
#features .overlay { background:url(<?php echo get_template_directory_uri(); ?>/images/overlay0.png);}
#portfolio{ background:url(<?php echo get_template_directory_uri(); ?>/images/demo/bg1.jpg) no-repeat fixed bottom center #e3633b; background-size:auto; color:#ffffff}
#portfolio .overlay { background:url(<?php echo get_template_directory_uri(); ?>/images/overlay3.png);}
#team{ background:url() no-repeat scroll top left #f6f6f6; background-size:cover; color:#333333}
#team .overlay { background:url(<?php echo get_template_directory_uri(); ?>/images/overlay0.png);}
#page-1{ background:url(<?php echo get_template_directory_uri(); ?>/images/demo/bg2.jpg) no-repeat fixed top center #1e73be; background-size:auto; color:}
#page-1 .overlay { background:url(<?php echo get_template_directory_uri(); ?>/images/overlay0.png);}
#testimonials{ background:url() no-repeat fixed top left #f6f6f6; background-size:cover; color:#333333}
#testimonials .overlay { background:url(<?php echo get_template_directory_uri(); ?>/images/overlay0.png);}
#blog{ background:url(<?php echo get_template_directory_uri(); ?>/images/demo/bg3.jpg) no-repeat fixed center center ; background-size:cover; color:#ffffff}
#blog .overlay { background:url(<?php echo get_template_directory_uri(); ?>/images/overlay3.png);}
#for-news-and-updates-subscribe-us{ background:url() no-repeat fixed top left #F6F6F6; background-size:cover; color:#333}
#contact{ background:url(<?php echo get_template_directory_uri(); ?>/images/demo/bg4.jpg) no-repeat scroll top left ; background-size:cover; color:#FFF}
#contact .overlay { background:url(<?php echo get_template_directory_uri(); ?>/images/overlay3.png);}
#google-map{ background:url() no-repeat scroll top left ; background-size:auto; color:}
#google-map .overlay { background:url(<?php echo get_template_directory_uri(); ?>/images/overlay0.png);}
#content{margin:0 !important}
#contact-us{ background:url(<?php echo get_template_directory_uri(); ?>/images/demo/bg3.jpg) no-repeat fixed bottom center #e3633b; background-size:auto; color:#ffffff}
#contact-us .overlay { background:url(<?php echo get_template_directory_uri(); ?>/images/overlay3.png);}
</style>